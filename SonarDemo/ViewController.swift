//
//  ViewController.swift
//  SonarDemo
//
//  Created by Pranay on 12/03/19.
//  Copyright © 2019 MyCompany. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

  override func viewDidLoad() {
    super.viewDidLoad()
    let session = URLSession.shared
    let url = URL(string: "https://www.google.com")!
    
    _ = session.dataTask(with: url) { data, response, error in
      
        print(error?.localizedDescription as Any)
    }
    // Do any additional setup after loading the view, typically from a nib.
  }
}
